## Inclusão de Metadados em Prescrição Eletrônica com Assinatura PDF via KMS (dados direto no backend)

Este é um exemplo integração dos serviços da API de assinatura médica com clientes baseados em tecnologia Java.

Este exemplo apresenta os passos necessários para a inclusão de metadados referentes à prescrição eletrônica em arquivos PDF, em conjunto com a assinatura do documento utilizando certificado em nuvem.

O documento será assinado em conjunto com a inclusão dos metadados.


### Autenticação, OID's e seus respectivos valores

Para entendimento dos requisitos de Autenticação, recomendamos que leia o artigo [Assinatura PDF Etapa Única](https://api-assinatura.bry.com.br/api-assinatura-digital#assinatura-pdf-etapa-unica).

Os valores correspondentes a cada tipo de OID podem ser encontrados no endereço [API Assinatura Prescrição](https://api-assinatura.bry.com.br/api-assinatura-medica#assinatura-da-prescricao).

### Formas de acesso ao Certificado em Nuvem

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **PIN** ou **TOKEN**. Para credenciais com tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem codificada em Base64, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem).

* *GovBR*: Certificado está armazenado na Plataforma de Assinatura Digital gov.br. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **GOVBR** e o Valor da Credencial corresponde ao token de acesso do usuário. É possível verificar um exemplo da geração deste token de acesso em nossa [API Autenticação GOV.BR](https://gitlab.com/brytecnologia-team/integracao/api-autenticacao-token-govbr/react/gestao-credenciais).

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| Authorization | Token de acesso (access_token), obtido no [BRyCloud](https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o token deve ser da pessoa  jurídica e para uso pessoal, da pessoa física. | AssinaturaPrescricaoEletronica.java
| kms_credencial | Valor da credencial para acesso ao certificado em nuvem. Se o tipo da credencial for "PIN", deve estar codificada em Base64 | AssinaturaPrescricaoEletronica.java
| kms_credencial_tipo | Tipo de crendecial utilizada para autenticação. [Disponíveis: PIN, TOKEN e GOVBR] | AssinaturaPrescricaoEletronica.java 


### Adquirir um certificado digital

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  


### Uso

Para execução da aplicação de exemplo, compile diretamente as classes ou importe o projeto em sua IDE de preferência.
Utilizamos o JRE versão 8 para desenvolvimento e execução.

[SpringBoot Web](https://spring.io/)

[JDK 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
