package br.com.bry.assinaturamedica;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AssinaturaPrescricaoEletronica {

	public static void main(String[] args) throws IOException {
	
		JSONObject jsonDadosAssinatura = autenticaDadosAssinatura();
		JSONObject jsonMetadados = adicionaMetadados();
		autenticaDadosAssinatura();
		adicionaMetadados();
	
		//Token de autenticação gerado no BRy Cloud
		String token = "";
		//Documento que será assinado
		File arquivo = new File("arquivos/teste.pdf"); 		
		
		RequestSpecification form = RestAssured.given()
				.header("Authorization", token)
				.header("kms_credencial", "") //Se for a PIN, o valor deve estar em base64
				.header("kms_credencial_tipo", "PIN") //PIN, TOKEN ou GOVBR
				.header("Content-Type", "multipart/form-data")
			.multiPart("documento[0]", arquivo)
			.multiPart("dados_assinatura", jsonDadosAssinatura.toString())
			.multiPart("metadados", jsonMetadados.toString());

			Response response = null;
			Response downloadDoc = null;
			
			try {
				//Assina
				response = form.post("https://hub2.bry.com.br/fw/v1/pdf/kms/lote/assinaturas");
				response.getBody().prettyPrint();
				//Pegando o link de download dentro do Json
				JSONObject json = new JSONObject(response.asString());
				JSONArray documentos = json.getJSONArray("documentos");
				JSONObject links = documentos.getJSONObject(0);
				JSONArray linksArray = links.getJSONArray("links");
				JSONObject urlObject = linksArray.getJSONObject(0);
				String urlDownload = urlObject.getString("href");
				//Download do PDF assinado
				downloadDoc = form.get(urlDownload);
			} catch (Exception e) {
				e.printStackTrace();
			}
			//Arquivo assinado
			FileOutputStream arquivoComMetadados = new FileOutputStream("arquivos/arquivoComMetadadosTeste.pdf");
			arquivoComMetadados.write(downloadDoc.body().asByteArray());
	}
	
	//Criando JSON de Dados de Assinatura
	private static JSONObject autenticaDadosAssinatura() throws IOException {
		JSONObject jsonDadosAssinatura = new JSONObject();
		jsonDadosAssinatura.put("signatario", "insira_CPF");//Ou use a UUID ("uuid", "")
		jsonDadosAssinatura.put("perfil", "BASICA");
		jsonDadosAssinatura.put("algoritmoHash", "SHA256");
		jsonDadosAssinatura.put("razao", "Razao");
		jsonDadosAssinatura.put("contato", "insira_email");
		jsonDadosAssinatura.put("local", "Florianopolis");
		jsonDadosAssinatura.put("tipoRestricao", "NENHUMA_RESTRICAO");
		jsonDadosAssinatura.put("tipoRetorno", "LINK");
		return jsonDadosAssinatura;
	}
	
	//Criando JSON com Metadados
	private static JSONObject adicionaMetadados() throws IOException {
		
		JSONObject jsonMetadados = new JSONObject();

		//Todas as OIDs variam de acordo com o Tipo de Profissional (vide links referenciados no readme)
		//Por exemplo: 2.16.76.1.4.2.3.3 indica que o valor será a Especialidade de um Profissional Farmacêutico, independente qual seja
		//No exemplo abaixo, está sendo informada uma Pescrição por um Farmaceutico com CRF=123, UF=SC e Especialidade=Farmaceutico

		//OID do Tipo do Documento (Atestado, Vacinação, Prescrição e etc) (valor nulo, quem identifica é o OID)
		jsonMetadados.put("2.16.76.1.12.1.2", "");
		
		//OID do Conselho (Médico, Farmacêutico ou Odontológico) e o Número de Registro Profissional
		jsonMetadados.put("2.16.76.1.4.2.3.1", "123");
		
		//OID referente à UF + qual UF
		jsonMetadados.put("2.16.76.1.4.2.3.2", "SC");
		
		//OID referente a Especialidade + qual especialidade
		jsonMetadados.put("2.16.76.1.4.2.3.3", "Farmaceutico");
		
		//Quando o profissional for médico, insere também os OIDs (vazios) de farmacêutico
		if (jsonMetadados.has("2.16.76.1.4.2.2.1")) {
			jsonMetadados.put("2.16.76.1.4.2.3.1", "");
			jsonMetadados.put("2.16.76.1.4.2.3.2", "");
			jsonMetadados.put("2.16.76.1.4.2.3.3", "");
		}
		return jsonMetadados;
	}
}
